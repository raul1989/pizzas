package com.mileva.focaccia.server.ejb.oh;

import java.util.HashMap;
import java.util.Map;
import javax.ejb.Singleton;

@Singleton
public class OrderMaps {
    public static Map<String, String> basicOrderMap;
    public static Map<String, String> promoOrderMap;

    static {
        basicOrderMap = new HashMap<String, String>();
        basicOrderMap.put("PersistOrderStep", "CommitOrderStep");
        basicOrderMap.put("CommitOrderStep", "DispatchOrderStep");
        basicOrderMap.put("DispatchOrderStep", "SendOrderStep");
        basicOrderMap.put("SendOrderStep", "DeliverOrderStep");
        basicOrderMap.put("DeliverOrderStep", "CloseOrderStep");
        basicOrderMap.put("CloseOrderStep", "");
    }
}