package com.mileva.focaccia.server.persistence;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
@Table(name="T_CUSTOMER")
@TableGenerator(
    name="customerIDGenerator",
    pkColumnName="SEQ_NAME",
    valueColumnName="SEQ_ID",
    pkColumnValue="Customer",
    initialValue=2048,
    allocationSize=1
)
public class Customer {
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="customerIDGenerator")
    private long id;
    private String alias;
    private String password;
    @OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true)
    private List<Order> orders;

    public Customer() {}

    public long getId() {return id;}
    public String getAlias() {return alias;}
    public String getPassword() {return password;}
    public List<Order> getOrders() {
        if (orders==null)
            orders = new LinkedList<Order>();
        return orders;
    }

    public void setId(long id) {this.id=id;}
    public void setAlias(String alias) {this.alias=alias;}
    public void setPassword(String password) {this.password=password;}
    public void setOrders(List<Order> orders) {this.orders=orders;}
}