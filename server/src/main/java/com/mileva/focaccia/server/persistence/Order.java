package com.mileva.focaccia.server.persistence;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@Table(name="T_ORDER")
@TableGenerator(
    name="orderIDGenerator",
    pkColumnName="SEQ_NAME",
    valueColumnName="SEQ_ID",
    pkColumnValue="Order",
    initialValue=2048,
    allocationSize=1
)
@Entity
@XmlRootElement
public class Order {
    public static final String STATUS_AC = "AC";
    public static final String STATUS_CO = "CO";
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="orderIDGenerator")
    private long id;
    private long temporalId;
    private String status;
    private Step currentStep;
    private boolean loadNextStep;
    @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true)
    private List<Step> steps;
    @OneToMany
    private List<Dish> dishes;
    @OneToMany
    private List<Beverage> beverages;

    public Order() {
    }

    public void setId(long id) {this.id=id;}
    public void setTemporalId(long temporalId) {this.temporalId=temporalId;}
    public void setStatus(String status) {this.status=status;}
    public void setCurrentStep(Step currentStep) {this.currentStep=currentStep;}
    public void setLoadNextStep(boolean loadNextStep) {this.loadNextStep=loadNextStep;}
    public void setSteps(List<Step> steps) {
        this.steps=steps;
    }
    public void addStep(Step step) {
        getSteps().add(step);
    }

    public long getId() {return id;}
    public long getTemporalId() {return temporalId;}
    public String getStatus() {return status;}
    public Step getCurrentStep() {return currentStep;}
    public boolean getLoadNextStep() { return loadNextStep;}
    public List<Step> getSteps() {
        if (steps==null)
            steps = new LinkedList<Step>();
        return steps;
    }
}