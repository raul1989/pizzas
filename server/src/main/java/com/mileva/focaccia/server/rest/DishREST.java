package com.mileva.focaccia.server.rest;

import com.mileva.focaccia.server.ejb.FoodFacade;
import com.mileva.focaccia.server.persistence.Dish;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

@Path("/menu/comidas")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class DishREST {
    @EJB
    private FoodFacade foodFacade;
    @Context
    private UriInfo uriInfo;

    @POST
    public Response createDish(Dish dish) {
        if (dish==null)
            throw new BadRequestException();
        foodFacade.persistEntity(dish);
        URI dishUri = uriInfo.getAbsolutePathBuilder().path(Long.toString(dish.getId())).build();
        return Response.created(dishUri).build();
    }

    @GET
    @Path("{id}")
    public Response getDish(@PathParam("id") String id) {
        Dish dish = foodFacade.getDishFindById(Long.parseLong(id.trim()));
        if (dish == null)
            throw new NotFoundException();
        return Response.ok(dish).build();
    }

    @GET
    public Response getDishes() {
        List<Dish> dishesList = foodFacade.getDishFindAll();
        Dish[] dishes = new Dish[dishesList.size()];
        for(int i=0; i<dishes.length; i++) {
            dishes[i]=dishesList.get(i);
        }
        return Response.ok(dishes).build();
    }

}