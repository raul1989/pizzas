package com.mileva.focaccia.server.persistence;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@NamedQueries({
    @NamedQuery(name="Dish.findAll", query="select d from Dish d"),
})
@XmlRootElement
@Table(name="T_DISH")
public class Dish extends Food {
    private List<String> ingredients;
    private String size;

    public Dish() {
    }

    public Dish(String name,
                String description,
                float price,
                List<String> ingredients,
                String size) {
        super(name, description, price);
        setIngredients(ingredients);
        setSize(size);
    }

    public void setIngredients(List<String> ingredients) {this.ingredients=ingredients;}
    public void setSize(String size) {this.size=size;}

    public List<String> getIngredients() {return ingredients;} 
    public String getSize() {return size;}
}