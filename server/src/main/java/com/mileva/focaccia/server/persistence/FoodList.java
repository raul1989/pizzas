package com.mileva.focaccia.server.persistence;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
 
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="food")
public class FoodList<T> {
    @XmlElements({
        @XmlElement(name="dishes", type=Dish.class),
        @XmlElement(name="beverages", type=Beverage.class)
    })
    private List<T> foodList = new ArrayList<T>();
     
    public FoodList() {}
 
    public FoodList(List<T> foodList) {
        this.foodList = foodList;
    }
 
    public List<T> getFoodList() {
        return foodList;
    }
 
    public void setFoodList(List<T> foodList) {
        this.foodList = foodList;
    }   
}