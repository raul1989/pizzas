package com.mileva.focaccia.server.ejb;

import com.mileva.focaccia.server.persistence.Dish;
import com.mileva.focaccia.server.persistence.Food;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class FoodFacade {
    @PersistenceContext(name="fomdbunit1.0")
    private EntityManager em;

    public <T> T persistEntity(T entity) {
        em.persist(entity);
        return entity;
    }

    public Dish getDishFindById(Long id) {
        return em.find(Dish.class, id);
    }

    public List<Dish> getDishFindAll() {
        return em.createNamedQuery("Dish.findAll", Dish.class).getResultList();
    }

    public <T extends Food> T getFoodFindById(Class<T> foodClass, Long id) {
        return em.find(foodClass, id);
    }

    public <T extends Food> List<T> getFoodFindAll(String nameClass, Class<T> foodClass) {
        return em.createNamedQuery(nameClass + ".findAll", foodClass).getResultList();
    }

}