package com.mileva.focaccia.server.persistence;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@NamedQueries({
    @NamedQuery(name="Beverage.findAll", query="select d from Beverage d"),
})
@XmlRootElement
@Table(name="T_BEVERAGE")
public class Beverage extends Food {
    private String flavor;
    private float capacity;

    public Beverage() {}

    public Beverage(String name,
                    String description,
                    float price,
                    String flavor,
                    float capacity) {
        super(name, description, price);
        setFlavor(flavor);
        setCapacity(capacity);
    }

    public void setFlavor(String flavor) {this.flavor=flavor;}
    public void setCapacity(float capacity) {this.capacity=capacity;}

    public String getFlavor() {return flavor;}
    public float getCapacity() {return capacity;}
}