package com.mileva.focaccia.server.persistence;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.Entity;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="T_STEP")
@TableGenerator(
    name="stepIDGenerator",
    pkColumnName="SEQ_NAME",
    valueColumnName="SEQ_ID",
    pkColumnValue="Step",
    initialValue=2048,
    allocationSize=1
)
public class Step {
    public static Map<String, String> friendlyNameMap;    
    public static final String STATUS_AC = "AC";
    public static final String STATUS_AW = "AW";
    public static final String STATUS_SF = "SF";
    public static final String STATUS_CO = "CO";
    @Id
    @GeneratedValue(strategy=GenerationType.TABLE, generator="stepIDGenerator")
    private long id;
    private String name;
    private String friendlyName;
    private String status;
    private int index;
    @Temporal(TemporalType.TIMESTAMP)
    private Date executionDate;

    static {
        friendlyNameMap = new HashMap<String, String>();
        friendlyNameMap.put("PersistOrderStep", "Tu orden ha sido recibida en el servidor de Focaccia");
        friendlyNameMap.put("CommitOrderStep", "Focaccia ha confirmado tu orden");
        friendlyNameMap.put("DispatchOrderStep", "Focaccia ha comenzado a preparar tu orden");
        friendlyNameMap.put("SendOrderStep", "Tu orden esta en camino a casa");
        friendlyNameMap.put("DeliverOrderStep", "Tu orden ha llegado a casa");
        friendlyNameMap.put("CloseOrderStep", "Tu orden ha sido entregada exitosamente");        
    }

    public Step() {}

    public Step(String name, String friendlyName, int index) {
        setName(name);
        setFriendlyName(friendlyName);
        setStatus(STATUS_AC);
        setIndex(index);
    }

    public void setId(long id) {this.id=id;}
    public void setName(String name) {this.name=name;}
    public void setFriendlyName(String friendlyName) {this.friendlyName=friendlyName;}
    public void setStatus(String status) {this.status=status;}
    public void setIndex(int index) {this.index=index;}
    public void setExecutionDate(Date executionDate) {this.executionDate=executionDate;}

    public long getId() {return id;}
    public String getName() {return name;}
    public String getFriendlyName() {return friendlyName;}
    public String getStatus() {return status;}
    public int getIndex() {return index;}
    public Date getExecutionDate() {return executionDate;}
}